# k6 snippets

This extension provides you with some code snippets that are useful when developing performance tests with k6.


### Supported Languages
- JavaScript (.js) 

</br>

### Snippets
| prefix      | Purpose                                              |
| ----------- | -----------                                          |
| `def`       | boiler plate for file that contains default function |
| `che`       | check statement                                      |
| `opt`       | options declaration                                  |
| `sle`       | sleep statement                                      |
| `mod`       | module declaration                                   |
| `gro`       | group declaration                                    |
| `get`       | http.get                                             |
| `post`      | http.post                                            |
| `put`       | http.put                                             |
| `del`       | http.del                                             |
| `head`      | headers declaration                                  |
| `notes`     | some comments on how to run                          |

</br>

### Warning
I made this project with the primary goal of learning how to make a VS code extension. I don’t really have any plans to update it. 
